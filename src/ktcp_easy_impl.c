/**
 * @file        ktcp_easy_impl.c
 * @author      leeopop
 * @date        Aug 2013
 * @version     $Revision: 1.00 $
 * @brief       Template for easy-KTCP project
 *
 * This is the main project template for transport layer implementation.
 * All functions below are linked with KENS kernel, so do not change the name or type of function.
 */

#include "ktcp_easy_impl.h"
#include "ktcp_easy_lib.h"

//suggesting header
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>

// Define NO_CC to lock congestion control and burst at will.
#define NO_CC

// Define FAST_RETRANSMIT to use TCP Reno mode.
//#define FAST_RETRANSMIT

size_t __min (size_t x, size_t y)
{
  return x>y?y:x;
}

// A linked list structure for managing bind information.
typedef struct __bind_element_struct
{
  struct in_addr sin_addr;
  unsigned short sin_port;
  struct __bind_element_struct *prev;
  struct __bind_element_struct *next;
} bind_element_t;

typedef enum __my_context_state_enum {
  STATE_NOT_READY,
  STATE_BOUND_ONLY,
  STATE_AWAIT_SV_ACK,
  STATE_AWAIT_SV_SYN,
  STATE_CONNECTED_IDLE,
  STATE_AWAIT_CL_SYN,
  STATE_AWAIT_CL_ACK,
  STATE_TEARDOWN_NOSEND_AWAIT_ACK,
  STATE_TEARDOWN_NOSEND_AWAIT_FIN,
  STATE_TEARDOWN_TIME_WAIT,
  STATE_TEARDOWN_NORECV_AWAIT_ACK,
  STATE_PENDING_DESTRUCTION
} socket_state_t;

typedef enum __congestion_control_state_enum {
  CONGESTION_CONTROL_BURST, // No congestion control
  CONGESTION_CONTROL_SLOW_START,
  CONGESTION_CONTROL_AIMD // Additive-increase, Multiplicative-decrease
} congestion_control_state_t;

// Bitmap: A helper structure for receiving buffer
typedef struct __bitmap_struct{
  unsigned int magic;
  size_t length;
  unsigned char* space;
} bitmap_t;

#define BITMAP_MAGIC_NUMBER 0xABCDEFAB

// Initialize a bitmap space.
void __bitmap_init (bitmap_t *target, size_t length){
  size_t size;
  size = (length - 1) >> 3;
  size += 1;
  char* bitspace = malloc(size);
  memset(bitspace, 0, size);
  target->magic = BITMAP_MAGIC_NUMBER;
  target->length = size;
  target->space = bitspace;
}

// Raise single flag of the bitmap.
void __bitmap_raise (bitmap_t *bitmap_context, size_t point)
{
  if (bitmap_context->magic != BITMAP_MAGIC_NUMBER)
    return;
  bitmap_context->space [point >> 3] |= 1 << (point & 0x7);
}

// Drop single flag of the bitmap.
void __bitmap_drop (bitmap_t *bitmap_context, size_t point)
{
  if (bitmap_context->magic != BITMAP_MAGIC_NUMBER)
    return;
  bitmap_context->space [point >> 3] &= 0xFF ^ (point & 0x7);
}

// Raise a block of flags of the bitmap.
void __bitmap_raise_region(bitmap_t *bitmap_context,size_t start,size_t length)
{
  if (bitmap_context->magic != BITMAP_MAGIC_NUMBER)
    return;
  size_t i;
  for (i = 0; i < length; i++)
    __bitmap_raise (bitmap_context, start + i);
}

// Drop a block of flags of the bitmap.
void __bitmap_drop_region(bitmap_t *bitmap_context,size_t start,size_t length)
{
  if (bitmap_context->magic != BITMAP_MAGIC_NUMBER)
    return;
  size_t i;
  for (i = 0; i < length; i++)
    __bitmap_drop (bitmap_context, start + i);
}

// Left-shift the bitmap by one bit.
void __bitmap_sl_one (bitmap_t *target)
{
  if (target->magic != BITMAP_MAGIC_NUMBER)
    return;
  unsigned int i;
  size_t bound = target->length;
  for (i = 0; i < bound; i++)
    target->space[i] = (target->space[i] >> 1) | (target->space[i + 1] & 1);
  target->space[i] >>= 1;
}

// Left-shift the bitmap by multiple bits.
void __bitmap_sl (bitmap_t *target, size_t count)
{
  if (target->magic != BITMAP_MAGIC_NUMBER)
    return;
  unsigned int i;
  if (count >> 3)
  {
    memmove (target->space, target->space + (count >> 3),
      target->length - (count >> 3));
    count &= 0x7;
  }
  for (i = 0; i < count; i++)
    __bitmap_sl_one (target);
}

// Count the number of consecutive bits raised from the start.
size_t __bitmap_consecutive (bitmap_t *target)
{
  size_t count = 0;
  size_t i;
  for (i = 0; i < target->length; i++)
  {
    if (target->space[i] != 0xFF)
    {
      if (target->space[i] & 1)
	if (target->space[i] & 0x3)
	  if (target->space[i] & 0x7)
	    if (target->space[i] & 0xF)
	      if (target->space[i] & 0x1F)
		if (target->space[i] & 0x3F)
		  if (target->space[i] & 0x7F)
		    return count + 7;
		  else
		    return count + 6;
		else
		  return count + 5;
              else
		return count + 4;
	    else
	      return count + 3;
	  else
	    return count + 2;
	else
	  return count + 1;
      else
	return count;
    }
    else
      count += 8;
  }
  return count;
}

// Free the space used by the bitmap.
void __bitmap_dispose (bitmap_t *target)
{
  free (target->space);
}

// A definition for a socket.
typedef struct __my_context_struct {
  int magic;
  bind_element_t *bound_addr;
  bind_element_t *cl_addr;
  socket_state_t state;
  unsigned int flags;

  // Backlog structure
  size_t max_backlog;
  struct __my_context_struct **backlogs;
  struct __my_context_struct *parent;

  // Numbers needed as a sender
  unsigned int last_confirmed_seq_number; // Last byte confirmed as ACKed
  unsigned int last_sent_seq_number; // Last byte which flied at least once
  size_t last_ack_count; // The number of ACKs received for this ACK number
  size_t remaining_bytes; // Bytes remaining in the buffer
  unsigned short rwnd_actual; // Recipient's receiving window
  bool sender_timer_registered; // Whether the timer is active
  void *sending_window; // Sender's buffer

  // Values for congestion control
  unsigned short rwnd; // Actually congestion window
  congestion_control_state_t congestion_state;
  unsigned short ssthresh; // Slow start threshold

  // Number needed for teardown
  int time_wait;

  // Fields needed as a recipient
  unsigned int expected_seq_number;
  void *receiving_buffer; // Recipient's buffer
  bitmap_t *receiving_buffer_bitmap;

  // Linked list structure
  struct __my_context_struct *prev;
  struct __my_context_struct *next;
} my_context_t;

#define SOCKET_FLAG_CHILD 0x1

// Magic number for determining whether the context is really a socket.
#define SOCKET_MAGIC_NUMBER 0xDEADBEEF

// A definition for a TCP header.
typedef struct __ktcp_header_struct {
  unsigned short src_port;
  unsigned short dest_port;
  unsigned int seq_number;
  unsigned int ack_number;
  unsigned short flags; // Data offset 4bit, flags 12bit
  unsigned short window;
  unsigned short checksum;
  unsigned short urgent_ptr;
} ktcp_header_t;
// Options and padding are NOT inserted currently.

#define MASK_DATA_OFFSET 0xF000
#define OFFSET_DATA_OFFSET 12
#define FLAG_NS  0x0100 // Nonce concealment protection
#define FLAG_CWR 0x0080 // Congestion Window Reduced
#define FLAG_ECE 0x0040 // Echo indicates
#define FLAG_URG 0x0020 // Urgent
#define FLAG_ACK 0x0010
#define FLAG_PSH 0x0008 // Push function
#define FLAG_RST 0x0004 // Reset the connection
#define FLAG_SYN 0x0002
#define FLAG_FIN 0x0001
#define DEFAULT_RECEIVE_WINDOW 3072
#define MSS 536 // According to KLMS, actual MSS is 536 bytes
#define SSTHRESH_INITIAL 65535

// Below are various timers. Unit is millisecond.
#define TIME_WAIT 240000 // Period for a dead socket to wait for ACK for FIN.
#define RESEND_PERIOD 500 // Period for assuming a sent packet to be lost.

void __initialize_ktcp_header (ktcp_header_t *subject)
{
  memset (subject, 0, sizeof (ktcp_header_t));
}

void __pack_ktcp_header (
  ktcp_header_t *subject,
  unsigned short src_port,
  unsigned short dest_port,
  unsigned int seq_number,
  unsigned int ack_number,
  unsigned short flags,
  unsigned short window,
  unsigned short urgent_ptr)
{
  subject->src_port = htons (src_port);
  subject->dest_port = htons (dest_port);
  subject->seq_number = htonl (seq_number);
  subject->ack_number = htonl (ack_number);
  subject->flags = htons (flags | (5 << OFFSET_DATA_OFFSET));
  // TODO: Variable header length due to options
  subject->window = htons (window);
  subject->checksum = 0; // Clear checksum until final
  subject->urgent_ptr = htons (urgent_ptr);
}

void __unpack_ktcp_header (
  ktcp_header_t *subject,
  unsigned short *src_port,
  unsigned short *dest_port,
  unsigned int *seq_number,
  unsigned int *ack_number,
  unsigned short *flags,
  unsigned short *window,
  unsigned short *checksum,
  unsigned short *urgent_ptr)
{
#define UNPACKS(x) if (x != NULL) *x = htons (subject->x)
#define UNPACKL(x) if (x != NULL) *x = htonl (subject->x)
  UNPACKS (src_port);
  UNPACKS (dest_port);
  UNPACKL (seq_number);
  UNPACKL (ack_number);
  UNPACKS (flags);
  UNPACKS (window);
  UNPACKS (checksum);
  UNPACKS (urgent_ptr);
#undef UNPACKL
#undef UNPACKS
}

unsigned short __1cadd (unsigned short x, unsigned short y)
{
  unsigned int result = (unsigned int) x + (unsigned int) y;
  if (result >> 16)
    result++;
  return (unsigned short) (result & 0xFFFF);
}

/*
unsigned short __calc_checksum (
  const ktcp_header_t *header,
  const void *data,
  size_t data_length,
  struct in_addr src_addr,
  struct in_addr dst_addr)
{
  unsigned int buffer = 0;
  // Calculate pseudoheader first.
  // Source address
  unsigned int s_addr = ntohl(src_addr.s_addr);
  buffer += s_addr & 0xFFFF;
  buffer += s_addr >> 16;
  // Destination address
  s_addr = ntohl (dst_addr.s_addr);
  buffer += s_addr & 0xFFFF;
  buffer += s_addr >> 16;
  // Protocol number
  buffer += 6;
  // TCP Length: Length of header + Length of data
  buffer += (data_length + sizeof (ktcp_header_t));
  // Add up from header.
  unsigned char i;
  unsigned short *csum_slot = (unsigned short *) header;
  for (i = sizeof (ktcp_header_t) / sizeof (unsigned short);
    i > 0; i--)
    buffer += ntohs (*(csum_slot++));
  // Add up from data field.
  csum_slot = (unsigned short *) data;
  for (; data_length > 1; data_length -= 2)
    buffer += ntohs (*(csum_slot++));
  if (data_length)
    buffer += (*((unsigned char *) csum_slot)) << 8;
  buffer = (buffer & 0xFFFF) + (buffer >> 16);
  unsigned short result = ((~buffer) & 0xFFFF);
  return result;
}
*/
// TODO: Checksum function not tested
// Note: Zero out the checksum field if you use it to
//       PRODUCE checksum.
unsigned short __calc_checksum (
  const ktcp_header_t *header,
  const void *data,
  size_t data_length,
  struct in_addr src_addr,
  struct in_addr dst_addr)
{
  unsigned short result = 0;
  // Calculate pseudoheader first.
  // Source address
  unsigned int s_addr = ntohl(src_addr.s_addr);
  result = __1cadd (result, (unsigned short)(s_addr & 0xFFFF));
  result = __1cadd (result, s_addr >> 16);
  // Destination address
  s_addr = ntohl (dst_addr.s_addr);
  result = __1cadd (result, s_addr & 0xFFFF);
  result = __1cadd (result, s_addr >> 16);
  // Protocol number
  result = __1cadd (result, 6);
  // TCP Length: Length of header + Length of data
  result = __1cadd (result, (data_length + sizeof (ktcp_header_t)));
  // Add up from header.
  unsigned char i;
  unsigned short *csum_slot = (unsigned short *) header;
  for (i = sizeof (ktcp_header_t) / sizeof (unsigned short);
    i > 0; i--)
    result = __1cadd (result, ntohs (*(csum_slot++)));
  // Add up from data field.
  csum_slot = (unsigned short *) data;
  for (; data_length > 1; data_length -= 2)
    result = __1cadd (result, ntohs (*(csum_slot++)));
  if (data_length)
    result = __1cadd (result, (*((unsigned char *) csum_slot)) << 8);
  return ~result;
}

// Apply calculated checksum.
void __apply_checksum (ktcp_header_t *header,
  void *data,
  size_t data_length,
  struct in_addr src_addr,
  struct in_addr dst_addr)
{
  // Clear the checksum field.
  header->checksum = 0;
  // Apply calculated checksum.
  header->checksum = htons (__calc_checksum (
    header, data, data_length, src_addr, dst_addr));
}

typedef struct
{
	ktcp_easy_impl my_syscall;

	//add global variables here
	ktcp_easy_lib* ktcp_lib;
	bind_element_t *bind_head;
	my_context_t *socket_head;
}global_context_t;

ktcp_easy_impl* my_startup(ktcp_easy_lib* lib);

static void my_shutdown(global_context_t* tcp_context); //finalize tcp context manager

static my_context my_open(global_context_t* tcp_context, int *err); //called when kopen is called
static void my_close(global_context_t* tcp_context, my_context handle,int *err); //handle: memory allocated from my_open
static bool my_bind(global_context_t* tcp_context, my_context handle, const struct sockaddr *my_addr, socklen_t addrlen,int *err);
static bool my_listen(global_context_t* tcp_context, my_context handle, int backlog, int *err);
static bool my_connect(global_context_t* tcp_context, my_context handle, const struct sockaddr *serv_addr, socklen_t addrlen, int *err);
static bool my_accept(global_context_t* tcp_context, my_context handle, int *err);
static bool my_getsockname(global_context_t* tcp_context, my_context handle, struct sockaddr *name, socklen_t *namelen, int *err);
static bool my_getpeername(global_context_t* tcp_context, my_context handle, struct sockaddr *name, socklen_t *namelen, int *err);
static void my_timer(global_context_t* tcp_context, my_context handle, int actual_called);
static void my_ip_dispatch_tcp(global_context_t* tcp_context, struct in_addr src_addr, struct in_addr dest_addr, const void * data, size_t data_size);
static int my_app_dispatch_tcp(global_context_t* tcp_context, my_context handle, const void* data, size_t data_size);




/**
 * @todo
 *
 * @brief
 * This function is called when KENS TCP layer is starting.
 *
 * @param ktcp_easy_lib library functions to use
 * @return prepared ktcp_easy_impl context for further use
 */
ktcp_easy_impl* my_startup(ktcp_easy_lib* lib)
{
	global_context_t* my_tcp = malloc(sizeof(global_context_t));

	my_tcp->my_syscall.shutdown = (void (*)(ktcp_easy_impl*))my_shutdown;
	my_tcp->my_syscall.open = (my_context (*)(ktcp_easy_impl*, int *))my_open;
	my_tcp->my_syscall.close = (void (*)(ktcp_easy_impl*, my_context,int *))my_close;
	my_tcp->my_syscall.bind = (bool (*)(ktcp_easy_impl*, my_context, const struct sockaddr *, socklen_t,int *))my_bind;
	my_tcp->my_syscall.listen = (bool (*)(ktcp_easy_impl*, my_context, int, int *))my_listen;
	my_tcp->my_syscall.connect = (bool (*)(ktcp_easy_impl*, my_context, const struct sockaddr *, socklen_t, int *))my_connect;
	my_tcp->my_syscall.accept = (bool (*)(ktcp_easy_impl*, my_context, int *))my_accept;
	my_tcp->my_syscall.getsockname = (bool (*)(ktcp_easy_impl*, my_context, struct sockaddr *, socklen_t *, int *))my_getsockname;
	my_tcp->my_syscall.getpeername = (bool (*)(ktcp_easy_impl*, my_context, struct sockaddr *, socklen_t *, int *))my_getpeername;
	my_tcp->my_syscall.timer = (void (*)(ktcp_easy_impl*, my_context, int))my_timer;
	my_tcp->my_syscall.ip_dispatch_tcp = (void (*)(ktcp_easy_impl*, struct in_addr, struct in_addr, const void *, size_t))my_ip_dispatch_tcp;
	my_tcp->my_syscall.app_dispatch_tcp = (int (*)(ktcp_easy_impl*, my_context, const void*, size_t))my_app_dispatch_tcp;

	my_tcp->ktcp_lib = lib;

	//add your initialization codes here
	my_tcp->bind_head = NULL;
	my_tcp->socket_head = NULL;

	// Set the random seed.
	srand (time (NULL));

	return (ktcp_easy_impl*)my_tcp;
}

/**
 * @todo
 *
 * @brief
 * This function is called when KENS TCP layer is exiting.
 *
 * @param tcp_context global context generated in my_startup. This includes KENS libraries and global variables.
 */
static void my_shutdown(global_context_t* tcp_context)
{
	free(tcp_context);
}

/**
 * @todo
 *
 * @brief
 * Mapped with 'ksocket'.
 *
 * @param tcp_context global context generated in my_startup.
 * @param err ERRNO value
 * @return TCP context data to be used (used to identify each application sockets)
 */

static my_context my_open(global_context_t* tcp_context, int *err)
{
  my_context_t *result = (my_context_t *)malloc (sizeof (my_context_t));
  if (result == NULL)
  {
    *err = ENOBUFS; // The system ran out of internal buffer space.
    return NULL;
  }
  memset (result, 0, sizeof (my_context_t));
  // Initialize values.
  result->magic = SOCKET_MAGIC_NUMBER;
  result->state = STATE_NOT_READY;
  result->ssthresh = SSTHRESH_INITIAL;
  // Initiate the linked list.
  result->next = tcp_context->socket_head;
  if (tcp_context->socket_head != NULL)
    tcp_context->socket_head->prev = result;
  tcp_context->socket_head = result;
  return result;
}

// Generic helper function.
// Fires a packed datagram through the socket.
bool __fire_datagram (global_context_t *tcp_context, my_context_t *socket,
  void *data, size_t length)
{
  // Dispatch packet to the destination IP
  int status = tcp_context->ktcp_lib->tcp_dispatch_ip (
    socket->bound_addr->sin_addr,
    socket->cl_addr->sin_addr,
    data,
    length
  );
  if (status == -1)
    // Notate that the operation was unsuccessful.
    return false;
  return true;
}

// Free any resource used by the socket, and remove socket from global sockets.
void __dispose_socket (global_context_t *tcp_context,my_context_t*socket,int*err)
{
  // Clear backlogs if any.
  if (socket->backlogs != NULL)
  {
    size_t i;
    int error_number;
    for (i = 0; i < socket->max_backlog; i++)
      my_close (tcp_context, socket->backlogs[i], &error_number);
    free (socket->backlogs);
  }
  if (socket->receiving_buffer != NULL)
    free (socket->receiving_buffer);
  if (socket->receiving_buffer_bitmap != NULL)
  {
    __bitmap_dispose (socket->receiving_buffer_bitmap);
    free (socket->receiving_buffer_bitmap);
  }
  // If applicable, clear the sending buffer.
  if (socket->sending_window != NULL)
    free (socket->sending_window);
  // If the socket was bound, then free the bound address.
  if (!(socket->flags & SOCKET_FLAG_CHILD) && socket->bound_addr != NULL)
  {
    // Slice the list.
#define addr_ptr socket->bound_addr
    if (addr_ptr->prev != NULL)
      addr_ptr->prev->next = addr_ptr->next;
    if (addr_ptr->next != NULL)
      addr_ptr->next->prev = addr_ptr->prev;
    // Was it the root?
    if (addr_ptr == tcp_context->bind_head)
      tcp_context->bind_head = addr_ptr->next;
    // Free the memory.
    free (addr_ptr);
#undef addr_ptr
  }
  else if ((socket->flags & SOCKET_FLAG_CHILD) &&
    socket->bound_addr != socket->parent->bound_addr)
  {
    // Child socket is not using rigid bind system, so just free.
    free (socket->bound_addr);
  }
  // Free the associated client address.
  if (socket->cl_addr != NULL)
    free (socket->cl_addr);
  // Slice itself from the socket linked list.
  if (socket->prev != NULL)
    socket->prev->next = socket->next;
  if (socket->next != NULL)
    socket->next->prev = socket->prev;
  if (socket == tcp_context->socket_head)
    tcp_context->socket_head = socket->next;
  // Prevent caching problem by clearing the magic number.
  socket->magic = ~SOCKET_MAGIC_NUMBER;
  *err = 0;
  // Free the socket's memory.
  free (socket);
}

// Helper function for TCP teardown
// Fire an appropriate FIN-ACK packet to the peer.
void __fire_fin_ack (global_context_t *tcp_context, my_context_t *socket)
{
  ktcp_header_t datagram;
  __initialize_ktcp_header (&datagram);
  __pack_ktcp_header (&datagram, ntohs (socket->bound_addr->sin_port),
    ntohs (socket->cl_addr->sin_port), socket->last_confirmed_seq_number,
    socket->expected_seq_number + 1, FLAG_FIN | FLAG_ACK,
    DEFAULT_RECEIVE_WINDOW, 0);
  __apply_checksum (&datagram, NULL, 0, socket->bound_addr->sin_addr,
    socket->cl_addr->sin_addr);
  __fire_datagram (tcp_context, socket, &datagram, sizeof (ktcp_header_t));
}

// Helper function for TCP teardown
// Fire an appropriate FIN packet to the peer.
void __fire_fin (global_context_t *tcp_context, my_context_t *socket)
{
  ktcp_header_t datagram;
  __initialize_ktcp_header (&datagram);
  __pack_ktcp_header (&datagram, ntohs (socket->bound_addr->sin_port),
    ntohs (socket->cl_addr->sin_port), socket->last_confirmed_seq_number, 0,
    FLAG_FIN, DEFAULT_RECEIVE_WINDOW, 0);
  __apply_checksum (&datagram, NULL, 0, socket->bound_addr->sin_addr,
    socket->cl_addr->sin_addr);
  __fire_datagram (tcp_context, socket, &datagram, sizeof (ktcp_header_t));
}

/**
 * @todo
 *
 * @brief
 * Mapped with 'kclose'.
 *
 * @param tcp_context global context generated in my_startup.
 * @param handle TCP context created via 'my_open'
 * @param err ERRNO value
 */
static void my_close(global_context_t* tcp_context, my_context handle,int *err)
{
  *err = 0;
  my_context_t *socket = (my_context_t *) handle;
  if (socket->magic != SOCKET_MAGIC_NUMBER)
  {
    // Not a socket.
    *err = EBADF;
    return;
  }
  // TODO: Do TCP teardown if applicable.
  switch (socket->state)
  {
  case STATE_CONNECTED_IDLE: // Initiate TCP teardown.
    // Send FIN packet to the opponent.
    __fire_fin (tcp_context, socket);
    // Change my state to appropriate one.
    socket->state = STATE_TEARDOWN_NOSEND_AWAIT_ACK;
    // Set timer for resend.
    tcp_context->ktcp_lib->tcp_register_timer (handle,
      tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD);
    return;
  case STATE_TEARDOWN_NOSEND_AWAIT_ACK:
  case STATE_TEARDOWN_NOSEND_AWAIT_FIN:
  case STATE_TEARDOWN_TIME_WAIT:
  case STATE_TEARDOWN_NORECV_AWAIT_ACK:
    // This socket is already under destruction process.
    // Nothing to do here.
    return;
  default: // Not dependent on the other participant; just clear memory.
    __dispose_socket (tcp_context, socket, err);
    return;
  }
}

/**
 * @todo
 *
 * @brief
 * Mapped with 'kbind'.
 *
 * @param tcp_context global context generated in my_startup.
 * @param handle TCP context created via 'my_open'
 * @param my_addr address of this socket
 * @param addrlen length of my_addr structure
 * @param err ERRNO value
 * @return whether this operation is successful
 */
static bool my_bind(global_context_t* tcp_context, my_context handle, const struct sockaddr *my_addr, socklen_t addrlen,int *err)
{
  // Check whether the "socket" is really a socket.
  my_context_t *socket = (my_context_t *)handle;
  if (socket->magic != SOCKET_MAGIC_NUMBER)
  {
    *err = EBADF; // handle is not a socket.
    return false;
  }
  // Check whether the socket is already bound.
  if (socket->bound_addr != NULL)
  {
    *err = EINVAL; // Socket already bound.
    return false;
  }
  // Convert the address into IPv4 address.
  struct sockaddr_in in_addr;
  memset (&in_addr, 0, sizeof (struct sockaddr_in));
  memcpy (&in_addr, my_addr, addrlen);

  // Check whether the address is malformed.
  if (in_addr.sin_family != AF_INET)
  {
    // Not an IPv4 address
    *err = EINVAL;
    return false;
  }
  // Check whether the given address already resides in the bind list.
  bind_element_t *iterator = tcp_context->bind_head;
  // Loop until the list runs out.
  while (iterator != NULL)
  {
    // Is the subject port already in use?
    if ((iterator->sin_addr.s_addr == INADDR_ANY ||
      iterator->sin_addr.s_addr == in_addr.sin_addr.s_addr) &&
      iterator->sin_port == in_addr.sin_port)
    {
      // Address already in use.
      *err = EADDRINUSE;
      return false;
    }
    // Iterate to the next element.
    iterator = iterator->next;
  }
  // Create a new element.
  iterator = (bind_element_t *) malloc (sizeof (bind_element_t));
  if (iterator == NULL)
  {
    // No memory
    *err = ENOBUFS;
    return false;
  }
  // Set the bound address and the port.
  iterator->sin_addr = in_addr.sin_addr;
  iterator->sin_port = in_addr.sin_port;
  // Connect the list.
  iterator->next = tcp_context->bind_head;
  iterator->prev = NULL;
  if (tcp_context->bind_head != NULL)
    tcp_context->bind_head->prev = iterator;

  // Set the new element as the new root.
  tcp_context->bind_head = iterator;

  // Mark the bound element as the associated bind.
  socket->bound_addr = iterator;
  socket->state = STATE_BOUND_ONLY;
  
  // Bind success.
  return true;
}

/**
 * @todo
 *
 * @brief
 * Mapped with 'klisten'.
 *
 * @param tcp_context global context generated in my_startup.
 * @param handle TCP context created via 'my_open'
 * @param backlog maximum number of concurrently opening connections
 * @param err ERRNO value
 * @return whether this operation is successful
 */
static bool my_listen(global_context_t* tcp_context, my_context handle, int backlog, int *err)
{
  // Convert the context into a socket.
  my_context_t *socket = (my_context_t *)handle;
  if (socket->magic != SOCKET_MAGIC_NUMBER)
  {
    // ENOTSOCK: Not a socket
    *err = ENOTSOCK;
    return false;
  }
  // Check whether the socket is bound.
  if (socket->bound_addr == NULL)
  {
    // EOPNOTSUPP: Operation not supported for an unbound socket.
    *err = EOPNOTSUPP;
    return false;
  }
  // TODO: Should we invoke EADDRINUSE?
  // Associate space for backlogs.
  socket->backlogs = malloc (backlog * sizeof (my_context_t *));
  socket->max_backlog = backlog;
  size_t i;
  for (i = 0; i < socket->max_backlog; i++)
    socket->backlogs[i] = NULL;
  // Now the socket awaits for client's SYN signal.
  socket->state = STATE_AWAIT_CL_SYN;
 
  // Listen complete.
  *err = 0;
  return true;
}




/**
 * @todo
 *
 * @brief
 * Mapped with 'kconnect'.
 *
 * @param tcp_context global context generated in my_startup.
 * @param handle TCP context created via 'my_open'
 * @param serv_addr remote address connecting to
 * @param addrlen length of serv_addr structure
 * @param err ERRNO value
 * @return whether this operation is successful
 */
static bool my_connect(global_context_t* tcp_context, my_context handle, const struct sockaddr *serv_addr, socklen_t addrlen, int *err)
{
  *err = 0;
  // Verify whether the socket is valid.
  my_context_t *socket = (my_context_t *)handle;
  if (socket->magic != SOCKET_MAGIC_NUMBER)
  {
    // ENOTSOCK: The target is not a socket.
    *err = ENOTSOCK;
    return false;
  }
  int status;
  int accomplish = 1;
  struct sockaddr_in src_addr;
  struct sockaddr_in dest_addr;
  ktcp_header_t head;
  unsigned int i;

  /* fill dest_addr */
  memset (&dest_addr, 0, sizeof (struct sockaddr_in));
  memcpy (&dest_addr, serv_addr, addrlen);

  // Is the socket bound?
  if (socket->bound_addr == NULL)
  {
    // Implicit bind: Dispatch IP address from the library,
    //                and assign a random port number.
    // First, fill the IP address from the library function.
    src_addr.sin_family = AF_INET;
    src_addr.sin_addr.s_addr =
	    tcp_context->ktcp_lib->ip_host_address
		    (dest_addr.sin_addr);
    if (src_addr.sin_addr.s_addr == INADDR_ANY) // No route
    {
      // ENETUNREACH: The target address is
      //              unreachable.
      *err = ENETUNREACH;
      return false;
    }
    // Now, fill the port by repeating bind() until it succeeds.
    for (i = 1024; i < 65536; i++) // From ports which are not well-known
    {
      src_addr.sin_port = (unsigned short) htons (i);
      if (my_bind (tcp_context, handle, &src_addr, sizeof (struct sockaddr_in),
	err))
      {
	*err = 0;
	break;
      }
    }
    // Socket unbound?
    if (i == 65536)
    {
      // EAGAIN: There are too many ports open.
      *err = EAGAIN;
      return false;
    }
  }
  else
  {
    // Fill the address space with the bound address.
    src_addr.sin_family = AF_INET;
    src_addr.sin_addr.s_addr = socket->bound_addr->sin_addr.s_addr;
    src_addr.sin_port = socket->bound_addr->sin_port;
  }
  // Fill the server address.
  socket->cl_addr = (bind_element_t *)malloc (sizeof (bind_element_t));
  if (socket->cl_addr == NULL)
  {
    // ENOBUFS: There is no such memory to store client address.
    *err = ENOBUFS;
    return false;
  }
  socket->cl_addr->sin_addr = dest_addr.sin_addr;
  socket->cl_addr->sin_port = dest_addr.sin_port;
  socket->cl_addr->prev = NULL;
  socket->cl_addr->next = NULL;

  /* fill the header */
  unsigned int rand_seq = rand(); // sequence number of random
  socket->last_confirmed_seq_number = rand_seq;
  __pack_ktcp_header (&head, ntohs (src_addr.sin_port),
    ntohs (dest_addr.sin_port), rand_seq, 0, FLAG_SYN, 
    DEFAULT_RECEIVE_WINDOW, 0);
  /*
  head.src_port = src_addr.sin_port;
  head.dest_port = src_addr.sin_port;
  head.seq_number = rand_seq;
  head.ack_number = 0;
  head.flags = FLAG_SYN;
  head.window = 0;
  head.urgent_ptr = 0;
  */

  /* fill the checksum */
  __apply_checksum(&head, NULL, 0, src_addr.sin_addr, dest_addr.sin_addr);

  // Let's do some "fire-and-forget" here.
  if (!__fire_datagram (tcp_context, socket, &head, sizeof (ktcp_header_t)))
  {
    // TODO: Is this error appropriate for this?
    // ENETUNREACH: The destination couldn't be reached.
    *err = ENETUNREACH;
    return false;
  }
  // After firing SYN, sequence number progresses by one.
  socket->last_confirmed_seq_number++;
  // Register a function for timeout.
  tcp_context->ktcp_lib->tcp_register_timer (
    socket,
    tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD
  );
  // The socket now awaits for server's SYN-ACK.
  socket->state = STATE_AWAIT_SV_ACK;
  return true; // my_ip_dispatch_tcp will wakeup this.
}

/**
 * @todo
 *
 * @brief
 * Mapped with 'kaccept'.
 * 'kaccept' is immediately blocked (my_accept is not blocked).
 * Even if 'kaccept' is called after connection is established, it is blocked.
 * 'kaccept' can be waken up via tcp_context->ktcp_lib->tcp_passive_open.
 *
 * @param tcp_context global context generated in my_startup.
 * @param handle TCP context created via 'my_open' (listening socket)
 * @param err ERRNO value
 * @return whether this operation is successful
 */
static bool my_accept(global_context_t* tcp_context, my_context handle, int *err)
{
  // Check whether the socket is a valid one.
  my_context_t *socket = (my_context_t *)handle;
  if (socket->magic != SOCKET_MAGIC_NUMBER)
  {
    // ENOTSOCK: Not a socket
    *err = ENOTSOCK;
    return false;
  }
  else if (socket->backlogs == NULL ||
    socket->state != STATE_AWAIT_CL_SYN)
  {
    // EINVAL: Socket is not listening for connections.
    *err = EINVAL;
    return false;
  }
  // Wait until something fills the backlog.
  size_t i;
  do
  {
    // Socket somehow went rogue
    if (socket->magic != SOCKET_MAGIC_NUMBER
      || socket->state == STATE_NOT_READY
      || socket->state == STATE_BOUND_ONLY
    )
    {
      // ECONNABORTED: Socket has aborted itself
      *err = ECONNABORTED;
      return false;
    }
    for (i = 0; i < socket->max_backlog; i++)
    {
      if (socket->backlogs[i] != NULL)
      {
	// Wakeup the underlying kaccept by firing the current socket.
	tcp_context->ktcp_lib->tcp_passive_open (
	  socket, socket->backlogs[i]
	);
	// Ensure that the child's parent is the given server socket.
	socket->backlogs[i]->parent = socket;
	// Separate the child from the backlog.
	socket->backlogs[i] = NULL;
	return true;
      }
    }
  } while (0);
  return true;
}

/**
 * @todo
 *
 * @brief
 * Mapped with 'kgetsockname'.
 *
 * @param tcp_context global context generated in my_startup.
 * @param handle TCP context created via 'my_open'
 * @param name address of socket address structure
 * Write my address here.
 *
 * @param namelen length of address structure
 * Write length of my address structure size here.
 * This value should be initialized with the actual size of 'name' structure.
 *
 * @param err ERRNO value
 * @return whether this operation is successful
 */
static bool my_getsockname(global_context_t* tcp_context, my_context handle, struct sockaddr *name, socklen_t *namelen, int *err)
{
  // Is the target a valid socket?
  my_context_t *socket = (my_context_t *) handle;
  if (socket->magic != SOCKET_MAGIC_NUMBER)
  {
    *err = ENOTSOCK; // Not a socket
    return false;
  }
  // If there is NO space, then just return.
  if (*namelen <= 0)
    return true;
  struct sockaddr_in name_buffer;
  // Zerofill the variable.
  memset (&name_buffer, 0, sizeof (struct sockaddr_in));
  name_buffer.sin_family = AF_INET; // Internet socket
  // Is the socket bound?
  if (socket->bound_addr != NULL)
  {
    // Write the bound information.
    name_buffer.sin_port = socket->bound_addr->sin_port;
    name_buffer.sin_addr = socket->bound_addr->sin_addr;
  }
  // Is the namelength longer than the actual socket info?
  if (*namelen > sizeof (struct sockaddr_in))
    *namelen = sizeof (struct sockaddr_in);
  // Fill the data.
  memcpy (name, &name_buffer, *namelen);
  // Return.
  return true;
}

/**
 * @todo
 *
 * @brief
 * Mapped with 'kgetpeername'.
 *
 * @param tcp_context global context generated in my_startup.
 * @param handle TCP context created via 'my_open'
 * @param name address of socket address structure
 * Write peer address here.
 *
 * @param namelen length of address structure
 * Write length of my address structure size here.
 * This value should be initialized with the actual size of 'name' structure.
 *
 * @param err ERRNO value
 * @return whether this operation is successful
 */
static bool my_getpeername(global_context_t* tcp_context, my_context handle, struct sockaddr *name, socklen_t *namelen, int *err)
{
  // Is the target a valid socket?
  my_context_t *socket = (my_context_t *) handle;
  if (socket->magic != SOCKET_MAGIC_NUMBER)
  {
    *err = ENOTSOCK; // not a socket
    return false;
  }
  // Is the target a connected socket?
  if (socket->cl_addr == NULL)
  {
    *err = ENOTCONN; // not connected
    return false;
  }
  // If there is NO space, then just return.
  if (*namelen <= 0)
    return true;
  struct sockaddr_in name_buffer;
  // Zerofill the variable.
  memset (&name_buffer, 0, sizeof (struct sockaddr_in));
  name_buffer.sin_family = AF_INET; // Internet socket
  // Write the connected information.
  name_buffer.sin_port = socket->cl_addr->sin_port;
  name_buffer.sin_addr = socket->cl_addr->sin_addr;
  // Is the namelength longer than the actual socket info?
  if (*namelen > sizeof (struct sockaddr_in))
    *namelen = sizeof (struct sockaddr_in);
  // Fill the data.
  memcpy (name, &name_buffer, *namelen);
  // Return.
  return true;
}

// Helper function of my_app_dispatch_tcp
// Fire ACK client packet.
bool __fire_ack (global_context_t *tcp_context,
  my_context_t *socket,
  unsigned int peer_syn_number)
{
  // Fill header.
  ktcp_header_t send_header;
  __pack_ktcp_header (&send_header, // Header to fill
    ntohs (socket->bound_addr->sin_port), // Srcport
    ntohs (socket->cl_addr->sin_port), // Destport
    socket->last_confirmed_seq_number, // Sequence number: Not applicable here
    peer_syn_number + 1, // Acknowledgement number
    FLAG_ACK, // Indicates ACK
    DEFAULT_RECEIVE_WINDOW, // rwnd; should fill in next session
    0 // Urgent pointer; currently no use
  );
  // Apply checksum.
  __apply_checksum (&send_header, NULL, 0,
    socket->bound_addr->sin_addr, socket->cl_addr->sin_addr);
  // Try to fire the packet until it is sent entirely.
  int actually_sent = 0;
  while (actually_sent != sizeof (ktcp_header_t))
  {
    actually_sent = tcp_context->ktcp_lib->tcp_dispatch_ip (
      socket->bound_addr->sin_addr, socket->cl_addr->sin_addr,
      &send_header, sizeof (ktcp_header_t));
    if (actually_sent == -1)
      return false;
  }
  return true;
}

// Helper function of my_app_dispatch_tcp
// Fire SYN-ACK server packet.
bool __fire_syn_ack (global_context_t *tcp_context,
  my_context_t *socket,
  unsigned int peer_syn_number,
  struct in_addr src_addr)
{
  // Fill header.
  ktcp_header_t send_header;
  __pack_ktcp_header (&send_header, // Header to fill
    ntohs (socket->bound_addr->sin_port), // Srcport
    ntohs (socket->cl_addr->sin_port), // Destport
    (socket->last_confirmed_seq_number), // Sequence number
    peer_syn_number + 1, // Acknowledgement number
    FLAG_SYN | FLAG_ACK, // Indicates SYN-ACK
    DEFAULT_RECEIVE_WINDOW, // rwnd; should fill in in next session
    0 // Urgent pointer; currently no use
  );
  // Apply checksum.
  __apply_checksum (&send_header, NULL, 0,
    src_addr /*socket->bound_addr->sin_addr*/, socket->cl_addr->sin_addr);
  // Try to fire the packet until it is sent entirely.
  int actually_sent = 0;
  while (actually_sent != sizeof (ktcp_header_t))
  {
    actually_sent = tcp_context->ktcp_lib->tcp_dispatch_ip (
      //socket->bound_addr->sin_addr, // DON'T USE THIS: This may be 0
      src_addr,
      socket->cl_addr->sin_addr,
      &send_header,
      sizeof (ktcp_header_t));
    if (actually_sent == -1)
      return false;
  }
  // If not errornous, then return true.
  // After firing SYN, my sequence number progresses by one.
  return true;
}

// TODO: Wait for first data and piggyback ACK onto it
/**
 * @todo
 *
 * @brief
 * Every time application calls 'write', this function is called.
 *
 * @param tcp_context global context generated in my_startup.
 * @param handle TCP context linked with application socket
 * @param dest_addr destination IP address (in network ordering)
 * @param data written data via 'write'
 * @param data_size size of data
 * @return actual written bytes (-1 means closed socket)
 */
static int my_app_dispatch_tcp(global_context_t* tcp_context, my_context handle, const void* data, size_t data_size)
{
  // Is it really a socket?
  my_context_t *socket = (my_context_t *)handle;
  if (socket->magic != SOCKET_MAGIC_NUMBER)
    return -1; // Not a socket
  // Is the socket connected?
  if (socket->state != STATE_CONNECTED_IDLE)
    return -1; // Not in legitimate state
  if (data_size == 0)
    return 0; // Nothing to send.
  size_t consumed = 0;
  // Is the remaining buffer smaller than the rwnd?
  // Should I send this data?
  if (socket->rwnd > socket->last_sent_seq_number -
    socket->last_confirmed_seq_number)
  {
    consumed = __min (data_size, socket->rwnd - (socket->last_sent_seq_number -
      socket->last_confirmed_seq_number));
    void *datagram_buffer = malloc (consumed + sizeof (ktcp_header_t));
    if (datagram_buffer == NULL)
      return -1; // Memory failure
    ktcp_header_t *hdr = (ktcp_header_t *) datagram_buffer;
    void *content = datagram_buffer + sizeof (ktcp_header_t);
    __initialize_ktcp_header (hdr);
    __pack_ktcp_header (hdr, ntohs (socket->bound_addr->sin_port),
      ntohs (socket->cl_addr->sin_port),
      socket->last_sent_seq_number,
      0, // ACK number is not meaningful here, but TODO: ACK piggybacking
      0, // Flags. TODO: ACK piggybacking
      DEFAULT_RECEIVE_WINDOW, // TODO: Window scaling
      0
    );
    __apply_checksum (hdr, data, consumed, socket->bound_addr->sin_addr,
      socket->cl_addr->sin_addr);
    memcpy (content, data, consumed);
    __fire_datagram (tcp_context, socket, datagram_buffer,
      sizeof (ktcp_header_t) + consumed);
    socket->last_sent_seq_number += consumed;
    free (datagram_buffer);
    // Set timer for resending.
    tcp_context->ktcp_lib->tcp_register_timer (socket,
      tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD);
  }
  // Try to expand the size of the buffer.
  void *new_buffer = realloc (socket->sending_window,
    data_size + socket->remaining_bytes);
  if (new_buffer == NULL)
    return consumed; // Memory issue
  else
    socket->sending_window = new_buffer;
  // Copy the data into the expanded part of the window.
  memcpy (socket->sending_window + socket->remaining_bytes, data, data_size);
  // Expand sending window.
  socket->remaining_bytes += data_size;

  // We have consumed every byte!
  return data_size;
}

// Helper function for TCP sending
// Send out the first unsent bytes.
void __send_remaining_data (global_context_t *tcp_context,my_context_t*socket)
{
  if (socket->last_sent_seq_number - socket->last_confirmed_seq_number <
    socket->rwnd &&
    socket->last_sent_seq_number - socket->last_confirmed_seq_number <
    socket->remaining_bytes)
  {
    // Assign a buffer for datagram.
    size_t payload_size = __min (socket->remaining_bytes -
      (socket->last_sent_seq_number - socket->last_confirmed_seq_number),
      socket->rwnd - (socket->last_sent_seq_number -
      socket->last_confirmed_seq_number)
    );
    void *datagram_buffer = malloc (sizeof (ktcp_header_t) + payload_size);
    if (datagram_buffer == NULL)
      return;
    ktcp_header_t *hdr = (ktcp_header_t *)datagram_buffer;
    void *content = datagram_buffer + sizeof (ktcp_header_t);
    __initialize_ktcp_header (hdr);
    __pack_ktcp_header (hdr, ntohs (socket->bound_addr->sin_port),
      ntohs (socket->cl_addr->sin_port),
      socket->last_sent_seq_number, 0, 0, DEFAULT_RECEIVE_WINDOW, 0
    );
    memcpy (content, socket->sending_window + 
      (socket->last_sent_seq_number - socket->last_confirmed_seq_number),
      payload_size);
    __apply_checksum (hdr, content, payload_size,
      socket->bound_addr->sin_addr, socket->cl_addr->sin_addr);
    __fire_datagram (tcp_context, socket, datagram_buffer, 
      sizeof (ktcp_header_t) + payload_size);
    free (datagram_buffer);
    socket->last_sent_seq_number += payload_size;
  }
  // Set timer for resend.
  tcp_context->ktcp_lib->tcp_register_timer (socket,
    tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD);
}

// Helper function for TCP sending
// Send out the first remaining bytes.
void __flush_sending_window (global_context_t *tcp_context,my_context_t*socket)
{
  if (socket->remaining_bytes > 0)
  {
    // Assign a buffer for datagram.
    size_t payload_size = __min (socket->remaining_bytes, socket->rwnd);
    void *datagram_buffer = malloc (sizeof (ktcp_header_t) + payload_size);
    if (datagram_buffer == NULL)
      return;
    ktcp_header_t *hdr = (ktcp_header_t *)datagram_buffer;
    void *content = datagram_buffer + sizeof (ktcp_header_t);
    __initialize_ktcp_header (hdr);
    __pack_ktcp_header (hdr, ntohs (socket->bound_addr->sin_port),
      ntohs (socket->cl_addr->sin_port),
      socket->last_confirmed_seq_number, 0, 0, DEFAULT_RECEIVE_WINDOW, 0
    );
    __apply_checksum (hdr, socket->sending_window, payload_size,
      socket->bound_addr->sin_addr, socket->cl_addr->sin_addr);
    memcpy (content, socket->sending_window, payload_size);
    __fire_datagram (tcp_context, socket, datagram_buffer, 
      sizeof (ktcp_header_t) + payload_size);
    free (datagram_buffer);
    // Set timer for resend.
    tcp_context->ktcp_lib->tcp_register_timer (socket,
      tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD);
  }
}

/**
 * @todo
 *
 * @brief
 * When ip packet is received, this callback function is called.
 * Most IP headers are removed, and only data part is passed.
 * However, source IP address and destination IP address are passed for header computation.
 *
 * @param tcp_context global context generated in my_startup.
 * @param src_addr source IP address (in network ordering)
 * @param dest_addr destination IP address (in network ordering)
 * @param data IP payload
 * @param data_size size of data
 */
static void my_ip_dispatch_tcp(global_context_t* tcp_context, struct in_addr src_addr, struct in_addr dest_addr, const void * data, size_t data_size)
{
  if (data_size < sizeof (ktcp_header_t))
    return; // That is not a KTCP packet.
  // First, unpack TCP header.
  ktcp_header_t hdr;
  memcpy (&hdr, data, sizeof (ktcp_header_t));
  void *tcp_data = (data + sizeof (ktcp_header_t));
  size_t tcp_data_size = data_size - sizeof (ktcp_header_t);
  // Validate checksum.
  unsigned short checksum = __calc_checksum (&hdr, tcp_data,
    tcp_data_size, src_addr, dest_addr);
  if (checksum) // Malformed data
  {
    return; // That is not a KTCP packet.
  }
  my_context_t *iterator = tcp_context->socket_head;
  unsigned int i;
  unsigned short flags = ntohs (hdr.flags) & (~MASK_DATA_OFFSET);
  unsigned int seq_number;
  int _errno;
  while (iterator != NULL)
  {
    // Is the port number and the host name valid for this packet?
    if (iterator->bound_addr != NULL)
    {
      // NOTE: Under current mechanism, the socket stores the NETWORK ORDER
      //       of the port number, not the HOST REPRESENTATION of the port
      //       number. Keep that in mind, and DO NOT USE ntohs to retrieve
      //       the actual port number.
      if ((iterator->bound_addr->sin_addr.s_addr == dest_addr.s_addr ||
	iterator->bound_addr->sin_addr.s_addr == INADDR_ANY) &&
	hdr.dest_port == iterator->bound_addr->sin_port)
      {
	// Check for statuses which don't matter about the source.
	if (iterator->state == STATE_AWAIT_CL_SYN)
	{
	  // Is the packet a connection request?
	  if ((flags & FLAG_SYN) && !(flags & FLAG_ACK))
	  {
	    // Is the server backlog full?
	    for (i = 0; i < iterator->max_backlog; i++)
	    {
	      if (iterator->backlogs[i] == NULL)
	      {
		// Create a new socket on this backlog.
		my_context_t *child_socket = my_open (tcp_context, &_errno);
		if (child_socket != NULL) // No error
		{
		  if (iterator->bound_addr->sin_addr.s_addr == INADDR_ANY)
		  {
		    // TODO: Address reuse
		    child_socket->bound_addr =
		      (bind_element_t *) malloc (sizeof (bind_element_t));
		    memset (child_socket->bound_addr, 0,
		      sizeof (bind_element_t));
		    child_socket->bound_addr->sin_addr.s_addr =
		      dest_addr.s_addr;
		    child_socket->bound_addr->sin_port = hdr.dest_port;
		  }
		  else
		    child_socket->bound_addr = iterator->bound_addr;
		  child_socket->state = STATE_AWAIT_CL_ACK;
		  child_socket->flags |= SOCKET_FLAG_CHILD;
		  child_socket->cl_addr = (bind_element_t *)
		    malloc (sizeof (bind_element_t));
		  if (child_socket->cl_addr == NULL) // No memory
		  {
		    my_close (tcp_context, child_socket, &_errno);
		    return;
		  }
		  child_socket->cl_addr->sin_addr = src_addr;
		  child_socket->cl_addr->sin_port = hdr.src_port;
		  child_socket->parent = iterator;
		  child_socket->rwnd_actual =
		    child_socket->rwnd = MSS;
		  child_socket->expected_seq_number = 
		    ntohl (hdr.seq_number) + 1;
		  unsigned short new_rwnd = ntohs (hdr.window);
		  if (new_rwnd > 0)
		    child_socket->rwnd_actual =
		      child_socket->rwnd = new_rwnd;
		  iterator->backlogs[i] = child_socket;
		  // Fire SYN and ACK to the client.
		  child_socket->last_confirmed_seq_number = rand();
		  while (!__fire_syn_ack (tcp_context,
		    child_socket,
		    ntohl (hdr.seq_number), dest_addr));
		  child_socket->last_confirmed_seq_number++;
		  // Register when to resend SYN-ACK.
		  tcp_context->ktcp_lib->tcp_register_timer (child_socket,
		    tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD);
		  // This child now waits for client's ACK.
		  child_socket->state = STATE_AWAIT_CL_ACK;
		  return;
		}
	      }
	    }
	  }
	}
	else if (iterator->cl_addr != NULL)
	{
	  if (iterator->cl_addr->sin_addr.s_addr == src_addr.s_addr &&
	    hdr.src_port == iterator->cl_addr->sin_port)
	  {
	    // Source is also valid.
	    // If there is a receive window with non-zero value,
	    // then adjust our receive window state.
	    unsigned short new_rwnd = ntohs (hdr.window);
	    if (new_rwnd > 0)
	    {
	      iterator->rwnd_actual = new_rwnd;
	      iterator->rwnd = __min (iterator->rwnd_actual, iterator->rwnd);
	    }
	    else
	    {
	      iterator->rwnd_actual = MSS;
	      iterator->rwnd = __min (iterator->rwnd_actual, iterator->rwnd);
	    }
	    switch (iterator->state)
	    {
	    case STATE_AWAIT_SV_ACK:
	      if (flags & FLAG_ACK)
	      {
		if (ntohl (hdr.ack_number) == iterator->last_confirmed_seq_number)
		{
		  // Change the server's state into "should send SYN."
		  iterator->state = STATE_AWAIT_SV_SYN;
		  // Unset my timer.
		  tcp_context->ktcp_lib->tcp_unregister_timer (iterator);
		}
	      }
	      // If server SYN is included here, then don't break.
	      // Otherwise, break. Just... await it.
	      if (!(flags & FLAG_SYN))
		return;
	    case STATE_AWAIT_SV_SYN:
	      // Is it validly formatted as a SYN packet?
	      if (flags & FLAG_SYN)
	      {
		// Pack and fire ACK packet to the server.
		__fire_ack (tcp_context, iterator,
		  ntohl (hdr.seq_number));
		iterator->expected_seq_number = ntohl (hdr.seq_number) + 1;
		iterator->last_sent_seq_number =
		  iterator->last_confirmed_seq_number;
		// This socket is now connected.
		iterator->receiving_buffer = malloc (DEFAULT_RECEIVE_WINDOW);
		iterator->receiving_buffer_bitmap = (bitmap_t *) malloc (
		  sizeof (bitmap_t));
		__bitmap_init (iterator->receiving_buffer_bitmap,
		  DEFAULT_RECEIVE_WINDOW);
		// Connection function may now disable the block and continue.
		iterator->state = STATE_CONNECTED_IDLE;
		// Initiate congestion control.
		iterator->rwnd = __min (iterator->rwnd_actual, MSS);
		iterator->congestion_state = CONGESTION_CONTROL_SLOW_START;
		// Wakeup the ksocket.
		if (tcp_context->ktcp_lib->tcp_active_open (iterator))
		{
		  return;
		}
	      }
	      return;
	    case STATE_AWAIT_CL_ACK:
	      // FYI: A valid ACK packet contains
	      // - ACK number of SYN number + 1
	      // - Only ACK flag up, not SYN flag
	      if ((flags & FLAG_ACK) && !(flags & FLAG_SYN))
	      {
		if (ntohl (hdr.ack_number) == iterator->last_confirmed_seq_number)
		{
		  // Unset my timer.
		  tcp_context->ktcp_lib->tcp_unregister_timer (iterator);
		  // Change the client's state into connected state.
		  iterator->state = STATE_CONNECTED_IDLE;
		  iterator->receiving_buffer = malloc (DEFAULT_RECEIVE_WINDOW);
		  iterator->receiving_buffer_bitmap = (bitmap_t *) malloc (
		    sizeof (bitmap_t));
		  __bitmap_init (iterator->receiving_buffer_bitmap,
		    DEFAULT_RECEIVE_WINDOW);
		  iterator->last_sent_seq_number =
		    iterator->last_confirmed_seq_number;
		  // Initiate congestion control.
		  iterator->rwnd = __min (iterator->rwnd_actual, MSS);
		  iterator->congestion_state = CONGESTION_CONTROL_SLOW_START;
		  // Try to make the kernel accept this socket.
		  if (tcp_context->ktcp_lib->tcp_passive_open (
		    iterator->parent, iterator))
		  {
		    // Separate the socket from the parent's backlog.
		    for (i = 0; i < iterator->parent->max_backlog; i++)
		      if (iterator->parent->backlogs[i] == iterator)
			iterator->parent->backlogs[i] = NULL;
		  }
		}
	      }
	      return;
	    case STATE_CONNECTED_IDLE:
	      // Is there valid data?
	      if (tcp_data_size > 0)
	      {
		seq_number = ntohl (hdr.seq_number);
		if (iterator->expected_seq_number - seq_number < tcp_data_size)
		{
		  // Adjust the offset to refuse redundant data.
		  tcp_data += (iterator->expected_seq_number - seq_number);
		  tcp_data_size -= (iterator->expected_seq_number - seq_number);
		  __bitmap_raise_region (iterator->receiving_buffer_bitmap,
		    0, __min (tcp_data_size, DEFAULT_RECEIVE_WINDOW));
		  int status;
		  // Copy the data into the receiving buffer.
		  memcpy (iterator->receiving_buffer, tcp_data, __min (
		    tcp_data_size, DEFAULT_RECEIVE_WINDOW)
		  );
		  tcp_data = iterator->receiving_buffer;
		  tcp_data_size = __bitmap_consecutive (
		    iterator->receiving_buffer_bitmap);
		  while(tcp_data_size > 0){
		    status = tcp_context->ktcp_lib->tcp_dispatch_app(
		      iterator, iterator->receiving_buffer, tcp_data_size);

		    if(status < 0){
		      // Application context already closed this socket.
		      // TODO: Appropriate action here
		      break;
		    }
		    iterator->expected_seq_number += status;
		    tcp_data += status;
		    tcp_data_size -= status;
		  }
		  memmove (iterator->receiving_buffer,
		    iterator->receiving_buffer + __bitmap_consecutive (
		      iterator->receiving_buffer_bitmap),
		    DEFAULT_RECEIVE_WINDOW - __bitmap_consecutive (
		      iterator->receiving_buffer_bitmap)
		  );
		  __bitmap_sl (iterator->receiving_buffer_bitmap,
		    __bitmap_consecutive (iterator->receiving_buffer_bitmap));
		  if (status >= 0)
		  {
		    // Send appropriate ACK packet for received data.
		    // Note: Do NOT send ACK here if the datagram has
		    // FIN flag up.
		    if (!(flags & FLAG_FIN))
		      __fire_ack (tcp_context, iterator,
			iterator->expected_seq_number - 1);
		  }
		}
		else if (seq_number - iterator->expected_seq_number <
		  DEFAULT_RECEIVE_WINDOW)
		{
		  // Out-of-order, but there is still something to salvage.
		  // Store only to my internal buffer.
		  memcpy (iterator->receiving_buffer, tcp_data, __min (
		    tcp_data_size, DEFAULT_RECEIVE_WINDOW - (
		      seq_number - iterator->expected_seq_number)
		  ));
		  __bitmap_raise_region (iterator->receiving_buffer_bitmap,
		    seq_number - iterator->expected_seq_number, __min 
		      (tcp_data_size, DEFAULT_RECEIVE_WINDOW - (seq_number -
		        iterator->expected_seq_number)
		    )
		  );
		}
		else // Either redundant or out-of-order; anyway send ACK.
		{
		  if (!(flags & FLAG_FIN))
		    __fire_ack (tcp_context, iterator,
		      iterator->expected_seq_number - 1);
		}
	      } // end if (tcp_data_size > 0)

	      if ((flags & FLAG_SYN))
	      {
		// My ACK to SYN-ACK wasn't sent properly.
		// But first, check whether this is a valid ACK packet,
		// instead of some random spoofing.
		if (ntohl (hdr.seq_number) == iterator->expected_seq_number - 1
		  && ntohl (hdr.ack_number) == iterator->last_confirmed_seq_number)
		{
		  // Resend ACK, as in response to old SYN.
		  __fire_ack (tcp_context, iterator,
		    ntohl (hdr.seq_number));
		}
	      }
	      else if (flags & FLAG_ACK)
	      {
		// Is this a duplicate ACK?
		if (ntohl (hdr.ack_number) == iterator->last_confirmed_seq_number)
		{
		  iterator->last_ack_count++;
		  if (iterator->last_ack_count >= 3) // Triple duplicate ACK
		  {
#ifndef NO_CC
#ifdef FAST_RETRANSMIT
		    // Prepare AIMD.
		    // TCP Reno: Start at ssthresh + 3 * MSS
		    iterator->congestion_state = CONGESTION_CONTROL_AIMD;
		    iterator->rwnd = __min (iterator->rwnd_actual,
		      iterator->ssthresh + 3 * MSS);
#else
		    // TCP Tahoe: Commence slow-start again
		    iterator->rwnd = __min (iterator->rwnd_actual, MSS);
#endif
#endif
		    // Immediately resend what is remaining in the buffer.
		    __flush_sending_window (tcp_context, iterator);
		    iterator->last_ack_count = 0;
		  }
		}
		// Does the ACK number seem legitimate?
		else if (ntohl (hdr.ack_number) -
		  iterator->last_confirmed_seq_number <=
		  __min (iterator->remaining_bytes, iterator->rwnd_actual)
		)
		{
		  // Collapse the acknowledged part.
		  memmove (iterator->sending_window, iterator->sending_window +
		    (ntohl (hdr.ack_number) - iterator->last_confirmed_seq_number),
		    iterator->remaining_bytes -
		    (ntohl (hdr.ack_number) - iterator->last_confirmed_seq_number)
		  );
		  if (iterator->remaining_bytes - (ntohl (hdr.ack_number) -
		    iterator->last_confirmed_seq_number) > 0)
		    realloc (iterator->sending_window, iterator->remaining_bytes
		      - (ntohl (hdr.ack_number) -
		      iterator->last_confirmed_seq_number));
		  else
		  {
		    free (iterator->sending_window);
		    iterator->sending_window = NULL;
		  }
#ifndef NO_CC
		  // Congestion control
		  switch (iterator->congestion_state)
		  {
		  case CONGESTION_CONTROL_SLOW_START:
		    // Slow start phase
		    // ACK increases my rwnd by MSS.
		    iterator->rwnd = __min (iterator->rwnd + MSS,
		      iterator->rwnd_actual);
		    // If my rwnd exceeds ssthresh, then change to AIMD.
		    if (iterator->rwnd >= iterator->ssthresh)
		      iterator->congestion_state = CONGESTION_CONTROL_AIMD;
		    break;
#ifdef FAST_RETRANSMIT
		  case CONGESTION_CONTROL_AIMD:
		    // AIMD Phase
		    // ACK increases my rwnd by (MSS)^2 / (cwnd).
		    iterator->rwnd = __min (iterator->rwnd + 
		      MSS * MSS / iterator->rwnd,
		      iterator->rwnd_actual);
		    break;
#endif
		  }
#endif
		  // Update the sender-side information.
		  iterator->remaining_bytes -=
		    (ntohl (hdr.ack_number) - iterator->last_confirmed_seq_number);
		  iterator->last_confirmed_seq_number = ntohl (hdr.ack_number);
		  tcp_context->ktcp_lib->tcp_unregister_timer (iterator);
		  iterator->sender_timer_registered = false;
		  iterator->last_ack_count = 1;

		  // Either unset timer, or reset it.
		  if (iterator->last_confirmed_seq_number ==
		    iterator->last_sent_seq_number)
		  {
		    // Client consumed everything I have.
		    tcp_context->ktcp_lib->tcp_unregister_timer (iterator);
		  }
		  else
		  {
		    // There is still unsent data remaining.
		    tcp_context->ktcp_lib->tcp_register_timer (iterator,
		      tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD);
		  }
		  // If there is pending data to be transmitted, then send it.
		  if (iterator->sending_window != NULL)
		    __send_remaining_data (tcp_context, iterator);
		}
	      } // end if (flags & FLAG_ACK)

	    case STATE_TEARDOWN_NORECV_AWAIT_ACK:
	      if (flags & FLAG_FIN)
	      {
		// Client either wants to terminate the connection(trickle-down
		// from STATE_CONNECTED_IDLE), or the client sent his
		// termination request again.
		if (iterator->expected_seq_number == ntohl (hdr.seq_number))
		{
		  __fire_fin_ack (tcp_context, iterator);
		  // Register timer for resend.
		  tcp_context->ktcp_lib->tcp_register_timer (iterator,
		    tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD);
		}
		else // What? But you didn't send me something!
		  __fire_ack (tcp_context, iterator,
		    iterator->expected_seq_number - 1);
	      }
	      if (iterator->state == STATE_CONNECTED_IDLE)
	      {
		if (flags & FLAG_FIN)
		  iterator->state = STATE_TEARDOWN_NORECV_AWAIT_ACK;
		return;
	      }
	      if (flags & FLAG_ACK)
	      {
		// Is the ACK number valid?
		if (ntohl (hdr.ack_number) == iterator->last_confirmed_seq_number + 1)
		{
		  // Pend this socket for destruction.
		  // Since this is not me who invoked this destruction,
		  // the socket must remain on the memory to react to
		  // unexpected read/write requests.
		  iterator->state = STATE_PENDING_DESTRUCTION;
		  // Unset timer.
		  tcp_context->ktcp_lib->tcp_unregister_timer (iterator);
		}
	      }
	      return;
	    case STATE_TEARDOWN_NOSEND_AWAIT_ACK:
	      if (flags & FLAG_ACK && ntohl (hdr.ack_number) == 
		iterator->last_confirmed_seq_number + 1)
	      {
		// Unset timer.
		tcp_context->ktcp_lib->tcp_unregister_timer (iterator);
		iterator->state = STATE_TEARDOWN_NOSEND_AWAIT_FIN;
		// FIN progresses sequence number by one.
		iterator->last_confirmed_seq_number++;
	      }
	      if (!(flags & FLAG_FIN))
		return;
	    case STATE_TEARDOWN_NOSEND_AWAIT_FIN:
	    case STATE_TEARDOWN_TIME_WAIT:
	      if ((flags & FLAG_FIN) && ntohl (hdr.seq_number) ==
		iterator->expected_seq_number)
	      {
		// Respond to the given FIN.
		__fire_ack (tcp_context, iterator, ntohl (hdr.seq_number));
		// Set grace period if it is not already set.
		if (iterator->state == STATE_TEARDOWN_NOSEND_AWAIT_FIN)
		  iterator->time_wait = tcp_context->ktcp_lib->tcp_get_mtime ()
		    + TIME_WAIT;
		// Set timer for next response, or termination of grace period.
		tcp_context->ktcp_lib->tcp_register_timer (iterator, __min (
		  iterator->time_wait, tcp_context->ktcp_lib->tcp_get_mtime ()+
		  TIME_WAIT
		));
	      }
	    }
	  }
	}
      }
    }
    iterator = iterator->next;
  }
}

/**
 * @todo
 *
 * @brief
 * This function is called when timer activated.
 * Each timer is bound to each context.
 *
 * @param tcp_context global context generated in my_startup.
 * @param handle TCP context bound to this timer

 * @param actual_called actual time this timer called (in mtime)
 */
static void my_timer(global_context_t* tcp_context, my_context handle, int actual_called)
{
  my_context_t *socket = (my_context_t *)handle;
  ktcp_header_t *head;
  if (socket->magic != SOCKET_MAGIC_NUMBER)
    return;
  int _errno;
  switch (socket->state)
  {
  case STATE_CONNECTED_IDLE:
#ifndef NO_CC
    // TODO: Timeout means significant loss, and this calls for
    //       reinitiation of slow start.
    // Store half the current window size as the ssthresh.
    socket->ssthresh = socket->rwnd >> 1;
    // This is bad... Fall back to slow start & MSS.
    socket->congestion_state = CONGESTION_CONTROL_SLOW_START;
    socket->rwnd = __min (MSS, socket->rwnd);
#endif
    // Resend.
    __flush_sending_window (tcp_context, socket);
    // Register a timer for resending again.
    tcp_context->ktcp_lib->tcp_register_timer (socket,
      tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD);
    return;
  case STATE_AWAIT_SV_ACK:
    // Resend SYN using the current sequence number.
    head = malloc (sizeof (ktcp_header_t));
    // Fill the header.
    __pack_ktcp_header (head, ntohs (socket->bound_addr->sin_port),
      ntohs (socket->cl_addr->sin_port), socket->last_confirmed_seq_number - 1,
      0, FLAG_SYN, DEFAULT_RECEIVE_WINDOW, 0);
    // Fill the checksum.
    __apply_checksum (head, NULL, 0, socket->bound_addr->sin_addr,
      socket->cl_addr->sin_addr);
    // Fire and forget.
    __fire_datagram (tcp_context, socket, head, sizeof (ktcp_header_t));
    // Register another timeout.
    tcp_context->ktcp_lib->tcp_register_timer (socket,
      tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD
    );
    return;
  case STATE_AWAIT_CL_ACK:
    // Resend SYN-ACK and restore the current state.
    socket->expected_seq_number--;
    socket->last_confirmed_seq_number--;
    __fire_syn_ack (tcp_context, socket, socket->expected_seq_number,
      socket->bound_addr->sin_addr);
    socket->expected_seq_number++;
    socket->last_confirmed_seq_number++;
    // Register next timer.
    tcp_context->ktcp_lib->tcp_register_timer (socket,
      tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD);
    return;
  case STATE_TEARDOWN_NORECV_AWAIT_ACK:
    // Resend FIN-ACK.
    __fire_fin_ack (tcp_context, socket);
    // Register timer for resend.
    tcp_context->ktcp_lib->tcp_register_timer (socket,
      tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD);
    return;
  case STATE_TEARDOWN_NOSEND_AWAIT_ACK:
    // Resend FIN.
    __fire_fin (tcp_context, socket);
    // Set timer for resend.
    tcp_context->ktcp_lib->tcp_register_timer (socket,
      tcp_context->ktcp_lib->tcp_get_mtime () + RESEND_PERIOD);
    return;
  case STATE_TEARDOWN_TIME_WAIT:
    if (actual_called >= socket->time_wait)
      // Destroy this socket.
      __dispose_socket (tcp_context, handle, &_errno);
    return;
  }
}

